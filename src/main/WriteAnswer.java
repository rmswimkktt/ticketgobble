package main;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


public class WriteAnswer {
	public static void writeAnswer(Answer answer){
		try(BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream("./answer.txt")))){
			writer.write(answer.calcCountryNum() + " " + answer.getJointCountrys());
			writer.newLine();
			writer.write("ENV: " + answer.getENV());
			writer.newLine();
			writer.write("POINT: " + answer.getPoint());
			writer.newLine();
			writer.write(answer.getComment());
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
