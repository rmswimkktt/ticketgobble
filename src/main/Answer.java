package main;

import java.util.ArrayList;

public class Answer {
	private ArrayList<String> countrys;
	private String ENV = "Java";
	private String point;
	private String comment;
	public Answer(ArrayList<String> countrys, String point, String comment) {
		super();
		this.countrys = countrys;
		this.point = point;
		this.comment = comment;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getENV() {
		return ENV;
	}
	public ArrayList<String> getCountrys() {
		return countrys;
	}
	public void setCountrys(ArrayList<String> countrys) {
		this.countrys = countrys;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public int calcCountryNum(){
		return countrys.size();
	}
	public String getJointCountrys(){
		String temp = "";
		for(String country : countrys){
			if(temp != ""){
				temp += " ";
			}
			temp += country;
		}
		return temp;
	}
}
