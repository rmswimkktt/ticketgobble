package main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;


public class LoadData {

	private static final String TICKETS_DATA = "./zip/tickets.txt";

	public static ArrayList<Ticket> read() {
		ArrayList<Ticket> temp = new ArrayList<>();
		try(BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(TICKETS_DATA)))){
			String line = null;
			while((line = bufferedReader.readLine()) != null){
				temp.add(ToIntegersConverter.Ticket(line));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	/**
	 * @author rmswimkkrr
	 * 日付形式のデータを数字(1月1日を1。12月31日を366)で表現する形式
	 */
	public static class ToIntegersConverter {
		public static Ticket Ticket(String string) {
			String ticketBit[] = string.split(" ");
			StartEnd startEnd = makeStartEnd(ticketBit[1]);
			return new Ticket(ticketBit[0], startEnd.getStart(), startEnd.getEnd());
		}
		
		private static StartEnd makeStartEnd(String string){
			String temp[] = string.split("-");
			return new StartEnd(temp[0], temp[1]);
		}

		public static class StartEnd{
			private static final int URU_YEAR = 2012;
			private static final int FIRST_MONTH = 1;
			private static final int FIRST_DATE = 1;
			private int start;
			private int end;
			public int getStart() {
				return start;
			}
			public void setStart(int start) {
				this.start = start;
			}
			public int getEnd() {
				return end;
			}
			public void setEnd(int end) {
				this.end = end;
			}
			private StartEnd(String start, String end) {
				super();
				this.start = convertDateToInt(start);
				this.end = convertDateToInt(end);

			}
			public int convertDateToInt(String start) {
				Calendar base = Calendar.getInstance();
				base.set(URU_YEAR, FIRST_MONTH - 1, FIRST_DATE);

				Calendar calendar = Calendar.getInstance();
				String startSplit[] = start.split("/");
				calendar.set(URU_YEAR, Integer.parseInt(startSplit[0]) - 1, Integer.parseInt(startSplit[1]));
				return calendar.get(Calendar.DAY_OF_YEAR) - base.get(Calendar.DAY_OF_YEAR) + 1;
			}
		}
	}
}
