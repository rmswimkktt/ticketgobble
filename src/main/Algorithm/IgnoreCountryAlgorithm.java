package main.Algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import main.Answer;
import main.Ticket;

public class IgnoreCountryAlgorithm implements Algorithm{
	private ArrayList<Ticket> answerTickets;
	private static String POINT = "最良の解には選択されることのないものは除外すること";
	private static String COMMENT = "最良の解を得るときには必ず選択されることのないチケットが存在する。そのようなチケットを除外するとともに、ツアーの開始順でソートを行う。その後は取り出した順にツアーの開始日が直前に選択して取り入れたもののツアー終了日より大きければ選択していくことを繰り返すことで、最善解を得ることが出来る。";
	@Override
	public void exec(ArrayList<Ticket> arrayTickets) {
		ArrayList<Ticket> tickets = new ArrayList<>(ignoreAlgorithm(removeIgnoreCountry(arrayTickets)));
		Collections.sort(tickets, new Comparator<Ticket>() {
			@Override
			public int compare(Ticket ticket1, Ticket ticket2){
				return ticket1.getCountry().compareTo(ticket2.getCountry());
			}});
		answerTickets = tickets;
	}
	public ArrayList<Ticket> ignoreAlgorithm(ArrayList<Ticket> removeIgnoreCountry) {
		ArrayList<Ticket> algTickets = new ArrayList<Ticket>();
		for(Ticket ticket : removeIgnoreCountry){
			if(algTickets.size() == 0){
				algTickets.add(ticket);
				continue;
			}
			if(algTickets.get(algTickets.size() - 1).getEnd() < ticket.getStart()){
				algTickets.add(ticket);
				continue;
			}
		}
		return algTickets;
	}
	@Override
	public void execDebug(ArrayList<Ticket> arrayTickets) {
		System.out.println(Common.convertTicketsToDebugString(answerTickets));
	}
	@Override
	public Answer getAnswer() {
		return new Answer(Common.convertTicketsToString(answerTickets), POINT, COMMENT);
	}
	public class InsertTicketInformation{
		private int addIndex;
		private int interval;
		private boolean inserted;
		private InsertTicketInformation(int addIndex, int interval,
				boolean inserted) {
			super();
			this.addIndex = addIndex;
			this.interval = interval;
			this.inserted = inserted;
		}
		private InsertTicketInformation() {
			super();
			addIndex = 0;
			interval = Integer.MAX_VALUE;
			inserted = false;
		}
			
		public boolean isInserted() {
			return inserted;
		}
		public void setInserted(boolean inserted) {
			this.inserted = inserted;
		}
		public void setInserted(){
			setInserted(true);
		}
		public int getAddIndex() {
			return addIndex;
		}
		public void setAddIndex(int addIndex) {
			this.addIndex = addIndex;
		}
		public int getInterval() {
			return interval;
		}
		public void setInterval(int interval) {
			this.interval = interval;
		}
		public boolean isLessThenInterval(Ticket choiceTicket, Ticket ticket) {
			return Math.abs(this.getInterval()) > Math.abs(choiceTicket.substractStart(ticket));
		}
		
	}
	/**
	 * @param possibleTickets
	 * @return
	 */
	public ArrayList<Ticket> removeIgnoreCountry(ArrayList<Ticket> possibleTickets) {
		ArrayList<Ticket> tickets = new ArrayList<>();
		for(Ticket choiceTicket : possibleTickets){
			InsertTicketInformation insertTicket = new InsertTicketInformation();
			for(int index = 0; index < tickets.size(); index++){
				Ticket ticket = tickets.get(index);
				if(ticket.isEqualStart(choiceTicket)){
					if(ticket.isLessThanEqualEnd(choiceTicket)){
						insertTicket.setInserted();
						break;
					}
					else{
						tickets.remove(index);
						tickets.add(index, choiceTicket);
						insertTicket.setInserted();
						break;
					}
				}
				if(ticket.isLessThanStart(choiceTicket)){
					if(ticket.isMoreThanEqualEnd(choiceTicket)){
						if(index + 1 < tickets.size() &&
								tickets.get(index + 1).isLessThanEqualStart(choiceTicket)){
							tickets.remove(index + 1);
						}
						tickets.remove(index);
						tickets.add(index, choiceTicket);
						insertTicket.setInserted();
						break;
					}
					else{
						insertTicket.setAddIndex(index);
						if(insertTicket.isLessThenInterval(choiceTicket, ticket)){
							insertTicket.setInterval(ticket.substractStart(choiceTicket));
							insertTicket.setAddIndex(index);
						}
					}
				}
				if(ticket.isMoreThanStart(choiceTicket)){
					if(ticket.isLessThanEqualEnd(choiceTicket)){
						insertTicket.setInserted();
						break;
					}
					else{
						if(insertTicket.isLessThenInterval(choiceTicket, ticket)){
							insertTicket.setInterval(ticket.substractStart(choiceTicket));
							insertTicket.setAddIndex(index);
						}
					}
				}
			}
			if(!insertTicket.inserted){
				tickets.add(calcInsertTicketIndex(insertTicket), choiceTicket);
			}
		}
		return tickets;
	}

	private int calcInsertTicketIndex(InsertTicketInformation insertTicket) {
		if(isNegative(insertTicket.getInterval())){
			return insertTicket.getAddIndex() + 1;
		}
		else{
			return insertTicket.getAddIndex();
		}
	}
	private boolean isNegative(int interval) {
		return interval < 0;
	}
}
