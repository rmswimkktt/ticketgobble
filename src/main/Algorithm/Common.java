package main.Algorithm;

import java.util.ArrayList;

import main.Ticket;

public class Common {
	public static ArrayList<String> convertTicketsToString(ArrayList<Ticket> selected) {
		ArrayList<String> string = new ArrayList<>();
		for(Ticket ticket : selected){
			string.add(ticket.getCountry());
		}
		return string;
	}
	
	public static String convertTicketsToDebugString(ArrayList<Ticket> selected){
		String string = "";
		for(Ticket ticket : selected){
			string += String.format("%s-%3d-%3d\r\n", ticket.getCountry(), ticket.getStart(), ticket.getEnd());
		}
		return string;
	}
}
