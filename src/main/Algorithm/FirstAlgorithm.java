package main.Algorithm;

import java.util.ArrayList;

import main.Answer;
import main.Ticket;

public class FirstAlgorithm implements Algorithm {
	private ArrayList<String> countrys = new ArrayList<>();
	private static String POINT = "期間が重複しているかの判断は2つの観点を比べるだけで可能";
	private static String COMMENT = "他のアルゴリズム実装前のたたき台です";

	@Override
	public void exec(ArrayList<Ticket> arrayTickets) {
		ArrayList<Ticket> selected = new ArrayList<>();
		for(Ticket ticket : arrayTickets){
			if(allowInsert(selected, ticket)){
				selected.add(ticket);
			}
		}
		countrys = Common.convertTicketsToString(selected);
	}
	

	public boolean allowInsert(ArrayList<Ticket> selected, Ticket ticket) {
		for(Ticket select : selected){
			if(IsDoubleBooking(ticket, select)){
				return false;
			}
		}
		return true;
	}

	public boolean IsDoubleBooking(Ticket ticket, Ticket select) {
		return !(select.getStart() > ticket.getEnd() || select.getEnd() < ticket.getStart());
	}


	@Override
	public Answer getAnswer() {
		return new Answer(countrys, POINT, COMMENT);
	}


	@Override
	public void execDebug(ArrayList<Ticket> arrayTickets) {
		// TODO Auto-generated method stub
	}

}
