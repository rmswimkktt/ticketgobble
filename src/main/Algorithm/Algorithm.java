package main.Algorithm;

import java.util.ArrayList;

import main.Answer;
import main.Ticket;

public interface Algorithm {
	Answer getAnswer();
	void exec(ArrayList<Ticket> arrayTickets);
	void execDebug(ArrayList<Ticket> arrayTickets);
}
