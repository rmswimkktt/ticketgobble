package main.Algorithm;

import java.util.ArrayList;

import main.LoadData;
import main.Ticket;
import main.WriteAnswer;

public class Facade {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Ticket> read = LoadData.read();
		Algorithm algorithm = new IgnoreCountryAlgorithm();
		algorithm.exec(read);
		WriteAnswer.writeAnswer(algorithm.getAnswer());
	}
}
