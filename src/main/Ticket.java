package main;

public class Ticket {
	private String country;
	private int start;
	private int end;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	@Override
	public String toString() {
		return "[" + country + ",\t" + start + ",\t" + end + "]";
	}
	public Ticket(String country, int start, int end) {
		super();
		this.country = country;
		this.start = start;
		this.end = end;
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Ticket)) return false;
		Ticket ticket = (Ticket)obj;
		if(this.getCountry().equals(ticket.getCountry()) &&
				this.getEnd() == ticket.getEnd() &&
				this.getStart() == ticket.getStart()){
			return true;
		}
		return false;
	}
	public boolean isEqualStart(Ticket choiceTicket) {
		return this.getStart() == choiceTicket.getStart();
	}
	public boolean isLessThanEqualEnd(Ticket choiceTicket) {
		return this.getEnd() <= choiceTicket.getEnd();
	}
	public boolean isLessThanStart(Ticket choiceTicket) {
		return this.getStart() < choiceTicket.getStart();
	}
	public boolean isMoreThanStart(Ticket choiceTicket) {
		return this.getStart() > choiceTicket.getStart();
	}
	public boolean isMoreThanEqualEnd(Ticket choiceTicket) {
		return this.getEnd() >= choiceTicket.getEnd();
	}
	public int substractStart(Ticket choiceTicket) {
		return this.getStart() - choiceTicket.getStart();
	}
	public boolean isMoreThanEnd(Ticket choiceTicket) {
		return this.getEnd() > choiceTicket.getEnd();
	}
	public boolean isEqualEnd(Ticket choiceTicket) {
		return this.getEnd() == choiceTicket.getEnd();
	}
	public boolean isLessThanEqualStart(Ticket choiceTicket) {
		return this.getStart() <= choiceTicket.getStart();
	}
}
