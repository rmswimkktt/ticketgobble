package test.main;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import main.Ticket;

import org.junit.Test;

public class TicketTest {

	@Test
	public void equalsが動作すること() {
		Ticket actual = new Ticket("ABC", 2, 3);
		Ticket expected = new Ticket("ABC", 2, 3);
		assertThat(actual, is(expected));
	}

}
