package test.main;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import main.LoadData;
import main.LoadData.ToIntegersConverter;
import main.Ticket;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class LoadDataTest {

	public static class クラス本体{
		@Test
		public void ticketsファイルの読み込みができるか1(){
			Ticket actual = LoadData.read().get(0);
			Ticket expected = new Ticket("Afghanistan", 108, 109);
			assertThat(actual, is(expected));
		}
	
		@Test
		public void ticketsファイルの読み込みができるか2(){
			Ticket actual = LoadData.read().get(51);
			Ticket expected = new Ticket("Georgia", 60, 62);
			assertThat(actual, is(expected));
		}
	}

	public static class インナークラス {
		@Test
		public void ticketsの一行がTicketモデルに変換できること1() {
			Ticket expected = new Ticket("Aruba", 1, 8);
			Ticket actual = ToIntegersConverter.Ticket("Aruba 1/1-1/8");
			assertThat(actual, is(expected));
		}

		@Test
		public void ticketsの一行がTicketモデルに変換できること2() {
			Ticket expected = new Ticket("China", 31, 32);
			Ticket actual = ToIntegersConverter.Ticket("China 1/31-2/1");
			assertThat(actual, is(expected));
		}

		@Test
		public void うるう日が考慮されていること1() {
			Ticket expected = new Ticket("Georgia", 59, 60);
			Ticket actual = ToIntegersConverter.Ticket("Georgia 2/28-2/29");
			assertThat(actual, is(expected));
		}

		@Test
		public void うるう日が考慮されていること2() {
			Ticket expected = new Ticket("Georgia", 60, 62);
			Ticket actual = ToIntegersConverter.Ticket("Georgia 2/29-3/2");
			assertThat(actual, is(expected));
		}
	}

}
