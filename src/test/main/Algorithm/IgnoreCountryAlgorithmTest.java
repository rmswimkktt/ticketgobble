package test.main.Algorithm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import main.Ticket;
import main.Algorithm.IgnoreCountryAlgorithm;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class IgnoreCountryAlgorithmTest {

	@RunWith(Enclosed.class)
	public static class 不要なツアーを削除するアルゴリズム{
		public static class 組み合わせテスト{
			@Test
			public void 不要な選択肢を削除する機能が動いているか1() {
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 5));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 4, 5));
				assertThat(actual, is(expected));		
			}
	
			@Test
			public void 不要な選択肢を削除する機能が動いているか2() {
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 5));
				arrayList.add(new Ticket("C", 5, 6));
				arrayList.add(new Ticket("D", 5, 7));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 4, 5));
				expected.add(new Ticket("C", 5, 6));
				assertThat(actual, is(expected));		
			}
			
			@Test
			public void 不要な選択肢を削除する機能が動いているか3() {
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 5));
				arrayList.add(new Ticket("C", 5, 6));
				arrayList.add(new Ticket("D", 5, 7));
				arrayList.add(new Ticket("E", 7, 9));
				arrayList.add(new Ticket("F", 8, 10));
				arrayList.add(new Ticket("G", 9, 11));
				arrayList.add(new Ticket("H", 9, 10));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 4, 5));
				expected.add(new Ticket("C", 5, 6));
				expected.add(new Ticket("E", 7, 9));
				expected.add(new Ticket("H", 9, 10));
				assertThat(actual, is(expected));		
			}
		}

		public static class 先頭未満{
			@Test
			public void かつ末尾超過(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 3, 7));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));		
			}
			
			@Test
			public void かつ末尾同一(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 3, 6));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));		
			}
			
			@Test
			public void かつ末尾未満(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 3, 5));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 3, 5));
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));
			}
			
			@Test
			public void かつ先頭同一(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 3, 4));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 3, 4));
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));
			}
	
			@Test
			public void かつ先頭未満(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 2, 3));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 2, 3));
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));
			}
		}
	
		public static class 先頭同一{
			@Test
			public void かつ末尾超過(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 7));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));		
			}
		
			@Test
			public void かつ末尾同一(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 6));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 6));
				assertThat(actual, is(expected));		
			}
		
			@Test
			public void かつ末尾未満(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 6));
				arrayList.add(new Ticket("B", 4, 5));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 4, 5));
				assertThat(actual, is(expected));		
			}
		}
	
		public static class 先頭より大きい{
			@Test
			public void かつ末尾超過(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 8));
				arrayList.add(new Ticket("B", 5, 9));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 8));
				expected.add(new Ticket("B", 5, 9));
				assertThat(actual, is(expected));		
			}
	
			@Test
			public void かつ末尾同一(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 8));
				arrayList.add(new Ticket("B", 5, 8));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 5, 8));
				assertThat(actual, is(expected));		
			}
	
			@Test
			public void かつ末尾未満(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 8));
				arrayList.add(new Ticket("B", 5, 7));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("B", 5, 7));
				assertThat(actual, is(expected));		
			}
		}
	
		public static class 末尾が先頭以下{
			@Test
			public void かつ末尾と先頭が同一(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 8));
				arrayList.add(new Ticket("B", 8, 9));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 8));
				expected.add(new Ticket("B", 8, 9));
				assertThat(actual, is(expected));		
			}
			
			@Test
			public void かつ末尾より先頭が大きい(){
				IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
				ArrayList<Ticket> arrayList = new ArrayList<>();
				arrayList.add(new Ticket("A", 4, 8));
				arrayList.add(new Ticket("B", 9, 10));
				ArrayList<Ticket> actual = algorithm.removeIgnoreCountry(arrayList);
				ArrayList<Ticket> expected = new ArrayList<>();
				expected.add(new Ticket("A", 4, 8));
				expected.add(new Ticket("B", 9, 10));
				assertThat(actual, is(expected));		
			}
		}
	}

	public static class アルゴリズムの本体{
		@Test
		public void テスト1(){
			IgnoreCountryAlgorithm algorithm = new IgnoreCountryAlgorithm();
			ArrayList<Ticket> arrayList = new ArrayList<>();
			arrayList.add(new Ticket("A", 1, 5));
			arrayList.add(new Ticket("B", 4, 8));
			arrayList.add(new Ticket("C", 5, 9));
			arrayList.add(new Ticket("D", 6, 10));
			arrayList.add(new Ticket("E", 7, 11));
			ArrayList<Ticket> actual = algorithm.ignoreAlgorithm(arrayList);
			ArrayList<Ticket> expected = new ArrayList<>();
			expected.add(new Ticket("A", 1, 5));
			expected.add(new Ticket("D", 6, 10));
			assertThat(actual, is(expected));
		}
	}
}
