package test.main.Algorithm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import main.Answer;
import main.Ticket;
import main.Algorithm.Algorithm;
import main.Algorithm.FirstAlgorithm;

import org.junit.Before;
import org.junit.Test;

public class FirstAlgorithmTest {
	private Ticket ticket;
	@Before
	public void before(){
		ticket = new Ticket("B", 3, 6);
	}
	@Test
	public void 重なりがないか判断1なし(){
		assertFalse(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 1, 2), ticket));
	}
	
	@Test
	public void 重なりがないか判断2なし(){
		assertFalse(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 7, 8), ticket));
	}
	
	@Test
	public void 重なりがないか判断3あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 6, 7), ticket));
	}
	
	@Test
	public void 重なりがないか判断4あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 5, 7), ticket));
	}
	
	@Test
	public void 重なりがないか判断5あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 5, 6), ticket));
	}
	
	@Test
	public void 重なりがないか判断6あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 4, 5), ticket));
	}
	
	@Test
	public void 重なりがないか判断7あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 3, 4), ticket));
	}
	
	@Test
	public void 重なりがないか判断8あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 2, 4), ticket));
	}
	
	@Test
	public void 重なりがないか判断9あり(){
		assertTrue(new FirstAlgorithm().IsDoubleBooking(new Ticket("A", 2, 3), ticket));
	}
	
	@Test
	public void 簡単なアルゴリズム1() {
		ArrayList<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket("A", 1, 2));
		tickets.add(new Ticket("B", 3, 4));
		tickets.add(new Ticket("C", 4, 5));
		Algorithm algorithm = new FirstAlgorithm();
		algorithm.exec(tickets);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("A");
		expected.add("B");
		assertThat(algorithm.getAnswer().getCountrys(), is(expected));
	}

	@Test
	public void 簡単なアルゴリズム2() {
		ArrayList<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket("A", 1, 2));
		tickets.add(new Ticket("B", 3, 4));
		tickets.add(new Ticket("C", 4, 5));
		tickets.add(new Ticket("D", 6, 9));
		Algorithm algorithm = new FirstAlgorithm();
		algorithm.exec(tickets);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("A");
		expected.add("B");
		expected.add("D");
		assertThat(algorithm.getAnswer().getCountrys(), is(expected));
	}

}
